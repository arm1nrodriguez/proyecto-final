export class Egresos {
  constructor(
    public descripcion: string,
    public monto: number,
    public tipo: string,
  ) {}
}

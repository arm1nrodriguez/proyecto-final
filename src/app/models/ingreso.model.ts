export class Ingresos {
  constructor(
    public descripcion: string,
    public monto: number,
    public tipo: string,
  ) {}
}

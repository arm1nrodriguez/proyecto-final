import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { InicioComponent } from './inicio/inicio.component';
import { IngresosComponent } from './ingresos/ingresos.component';
import { EgresosComponent } from './egresos/egresos.component';
import {AngularMaterialModule} from "../../angular-material/angular-material.module";
import {NgChartsModule} from "ng2-charts";
import {ReactiveFormsModule} from "@angular/forms";
import { DetalleComponent } from './detalle/detalle.component';
import { DocumentosComponent } from './documentos/documentos.component';
import {NgDropFilesDirective} from "../../directives/ng-drop-files.directive";
import { ArchivosComponent } from './archivos/archivos.component';
import {NgxChartsModule} from "@swimlane/ngx-charts";
import {CurrencyMaskModule} from "ng2-currency-mask";


@NgModule({
  declarations: [
    DashboardComponent,
    InicioComponent,
    IngresosComponent,
    EgresosComponent,
    DetalleComponent,
    DocumentosComponent,
    NgDropFilesDirective,
    ArchivosComponent,

  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    AngularMaterialModule,
    NgChartsModule,
    ReactiveFormsModule,
    NgxChartsModule,
    CurrencyMaskModule
  ]
})
export class DashboardModule { }

import {createAction, props} from "@ngrx/store";
import {Egresos} from "../../../models/egresos.model";

export const setItems     = createAction('[EGRESO] SET ITEMS ', props<{ items: Egresos[]}>());
export const unsetItems = createAction('[EGRESO] UNSET ITEMS ');

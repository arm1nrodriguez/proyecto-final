import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Ingresos} from "../../../models/ingreso.model";
import {EgresoService} from "../../../services/egreso.service";
import Swal from "sweetalert2";
import {Subscription} from "rxjs";
import {Store} from "@ngrx/store";
import {AppState} from "../../../app.state";
import * as actions from "../../ui.actions";

@Component({
  selector: 'app-egresos',
  templateUrl: './egresos.component.html',
  styleUrls: ['./egresos.component.scss']
})
export class EgresosComponent implements OnInit, OnDestroy {

  egresosForm:     FormGroup;
  tipo                       = 'egreso';
  cargando              = false;
  loadingSub:         Subscription;

  constructor(private fb: FormBuilder,
                      private egresoService: EgresoService,
                      private store: Store<AppState>) { }

  ngOnInit(): void {
    this.loadingSub = this.store.select('ui').subscribe((ui) => this.cargando = ui.isLoading);
    this.egresosForm = this.fb.group({
      concepto: ['',[Validators.required]],
      monto: ['', Validators.required],
    });
  }

  ngOnDestroy() {
    this.loadingSub.unsubscribe();
  }

  egresos(){
    this.store.dispatch(actions.isLoading());
    const {concepto, monto} = this.egresosForm.value
    const egreso = new Ingresos(concepto, monto, this.tipo);
    this.egresoService.crearEgreso(egreso).then(() => {
      this.egresosForm.reset();
      this.store.dispatch(actions.stopLoading());
      Swal.fire('Egreso Creado', concepto, 'success')})
      .catch(err => {
        this.store.dispatch(actions.stopLoading());
        Swal.fire('Error', err.message, 'error')});
  }
}

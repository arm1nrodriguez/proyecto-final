
import { createReducer, on } from '@ngrx/store';
import * as action from './egreso.actions';
import {Egresos} from "../../../models/egresos.model";

export interface State {
  items: Egresos[];
}


export const initialState: State = {
  // @ts-ignore
  items: [],
}



export const _egresoReducer = createReducer(initialState,
  on( action.setItems, (state, { items }) => ({ ...state, items: [ ...items] })),
  // @ts-ignore
  on( action.unsetItems, state => ({ ...state, items: [] })),
);

// @ts-ignore
export const egresoReducer = (state, action) => _egresoReducer(state, action);

import { Component, OnInit } from '@angular/core';
import {AuthService} from "../../services/auth.service";
import {Router} from "@angular/router";
import {filter, Subscription} from "rxjs";
import * as ingresosActions from "./ingresos/ingreso.actions";
import * as egresosActions from "./egresos/egreso.actions";
import {IngresoService} from "../../services/ingreso.service";
import {EgresoService} from "../../services/egreso.service";
import {Store} from "@ngrx/store";
import {AppState} from "../../app.state";
import * as actionIngresos from "./ingresos/ingreso.actions";
import * as actionEgresos from "./egresos/egreso.actions";
import * as actionFiles from "./archivos/archivo.actions";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  userSub           :Subscription;
  ingresoSub      :Subscription;
  egresoSub       :Subscription;
  fileSub              :Subscription;

  constructor(private authService: AuthService,
              private store: Store<AppState>,
              private router: Router,
              private ingresoService: IngresoService,
              private egresoService: EgresoService ) { }

  ngOnInit(): void {

    this.userSub =  this.store.select('user').pipe(
      filter( auth => auth.user !== null)
    ).subscribe(({user}) => {
      this.ingresoSub = this.ingresoService.initIngresoListener(user.uid).subscribe((ingresos ) => {
        this.store.dispatch(ingresosActions.setItems({items: ingresos}));
      });
      this.egresoSub = this.egresoService.initEgresoListener(user.uid).subscribe((egresos) => {
        this.store.dispatch(egresosActions.setItems({items: egresos}));
      });
    });
  }

  ngOnDestroy() {

    if( this.userSub){
      this.userSub.unsubscribe();
    }
    if( this.ingresoSub){
      this.ingresoSub.unsubscribe();
    }
    if( this.egresoSub) {
      this.egresoSub.unsubscribe();
    }


  }

  logout(){
    this.authService.logout().then(()=>{
      this.router.navigate(['/']);
      this.store.dispatch(actionIngresos.unsetItems());
      this.store.dispatch(actionEgresos.unsetItems());
      this.store.dispatch(actionFiles.unsetFiles());
    });
  }

}

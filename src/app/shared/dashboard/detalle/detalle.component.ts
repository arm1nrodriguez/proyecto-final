import {Component, OnDestroy, OnInit} from '@angular/core';
import {Store} from "@ngrx/store";
import {AppState} from "../../../app.state";
import {Ingresos} from "../../../models/ingreso.model";
import {filter, Subscription} from "rxjs";
import * as ingresosActions from "../ingresos/ingreso.actions";
import * as egresosActions from "../egresos/egreso.actions";
import {IngresoService} from "../../../services/ingreso.service";
import {EgresoService} from "../../../services/egreso.service";
import {Egresos} from "../../../models/egresos.model";
import Swal from "sweetalert2";

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.component.html',
  styleUrls: ['./detalle.component.scss']
})
export class DetalleComponent implements OnInit, OnDestroy {
  userSub           :Subscription;
  ingresoSub      :Subscription;
  egresoSub       :Subscription;
  displayedColumns: string[] = ['descripcion', 'monto', 'tipo' , 'eliminar'];
  ingresos           : Ingresos[] = [];
  egresos            : Egresos[] = [];

  constructor(private store: Store<AppState>,  private ingresoService: IngresoService,
              private egresoService: EgresoService,) { }

  ngOnInit(): void {
    /*
    this.userSub =  this.store.select('user').pipe(
      filter( auth => auth.user !== null)
    ).subscribe(({user}) => {
      this.ingresoSub = this.ingresoService.initIngresoListener(user.uid).subscribe((ingresos ) => {
        this.store.dispatch(ingresosActions.setItems({items: ingresos}));
      });
      this.egresoSub = this.egresoService.initEgresoListener(user.uid).subscribe((egresos) => {
        this.store.dispatch(egresosActions.setItems({items: egresos}));
      });
    });

     */

    this.store.select('ingreso').subscribe(({items}) => this.ingresos = items);
    this.store.select('egreso').subscribe(({items}) => this.egresos = items);
  }

  ngOnDestroy() {
   // this.userSub.unsubscribe();
   // this.ingresoSub.unsubscribe();
    // this.egresoSub.unsubscribe();
  }
  eliminar(id, tipo){

    if (tipo === 'ingreso'){
      this.ingresoService.eliminarIngreso(id)
        .then(()=> Swal.fire('Eliminado', 'Ingreso Eliminado', 'success'))
        .catch(err => Swal.fire ('Error', err.message, 'error'))
    } else {
      this.egresoService.eliminarEgreso(id)
        .then(()=> Swal.fire('Eliminado', 'Egreso Eliminado', 'success'))
        .catch(err => Swal.fire ('Error', err.message, 'error'))
    }

  }
}

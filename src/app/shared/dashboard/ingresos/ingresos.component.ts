import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Ingresos} from "../../../models/ingreso.model";
import {IngresoService} from "../../../services/ingreso.service";
import Swal from "sweetalert2";
import {Store} from "@ngrx/store";
import {AppState} from "../../../app.state";
import * as actions  from "../../ui.actions";
import {Subscription} from "rxjs";


@Component({
  selector: 'app-ingresos',
  templateUrl: './ingresos.component.html',
  styleUrls: ['./ingresos.component.scss']
})
export class IngresosComponent implements OnInit, OnDestroy {

  ingresosForm:   FormGroup;
  tipo                     = 'ingreso';
  cargando            = false;
  loadingSub:       Subscription;

  constructor(private fb: FormBuilder,
                      private ingresoService: IngresoService,
                      private store: Store<AppState>) { }

  ngOnInit(): void {
    this.loadingSub = this.store.select('ui').subscribe((ui) => this.cargando = ui.isLoading);
    this.ingresosForm = this.fb.group({
      concepto: ['',[Validators.required]],
      monto: ['', Validators.required],
    });
  }

  ngOnDestroy() {
    this.loadingSub.unsubscribe();
  }

  ingresos(){
    this.store.dispatch(actions.isLoading());
    const {concepto, monto} = this.ingresosForm.value;
    const ingreso = new Ingresos(concepto, monto, this.tipo);
    this.ingresoService.crearIngreso(ingreso).then(() => {
      this.ingresosForm.reset();
      this.store.dispatch(actions.stopLoading());
      Swal.fire('Ingreso Creado', concepto, 'success')})
      .catch(err => {
          this.store.dispatch(actions.stopLoading());
        Swal.fire('Error', err.message, 'error')
      }
      );
  }
}


import { createReducer, on } from '@ngrx/store';
import * as action from './ingreso.actions';
import {Ingresos} from "../../../models/ingreso.model";

export interface State {
  items: Ingresos[];
}


export const initialState: State = {
  // @ts-ignore
  items: [],
}



export const _ingresoReducer = createReducer(initialState,
  on( action.setItems, (state, { items }) => ({ ...state, items: [ ...items] })),
  // @ts-ignore
  on( action.unsetItems, state => ({ ...state, items: [] })),
);

// @ts-ignore
export const ingresoReducer = (state, action) => _ingresoReducer(state, action);

import {createAction, props} from "@ngrx/store";
import {Ingresos} from "../../../models/ingreso.model";

export const setItems     = createAction('[INGRESO] SET ITEMS ', props<{ items: Ingresos[]}>());
export const unsetItems = createAction('[INGRESO] UNSET ITEMS ');

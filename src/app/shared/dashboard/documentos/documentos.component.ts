import { Component, OnInit } from '@angular/core';
import {FileItem} from "../../../models/file-item";
import {UploadFilesService} from "../../../services/upload-files.service";

@Component({
  selector: 'app-documentos',
  templateUrl: './documentos.component.html',
  styleUrls: ['./documentos.component.scss']
})
export class DocumentosComponent implements OnInit {

  isOverElement = false;
  files: FileItem[] = [];

  constructor(public uploadFilesService: UploadFilesService) { }

  ngOnInit(): void {
  }


  uploadFiles(){
    this.uploadFilesService.uploadFiles(this.files);
  }

  clean() {
    this.files = [];
  }

}

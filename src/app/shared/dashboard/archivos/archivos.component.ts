import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import {FilesService} from "../../../services/files.service";
import {filter, Subscription} from "rxjs";
import {Store} from "@ngrx/store";
import {AppState} from "../../../app.state";
import * as filesActions from "../archivos/archivo.actions";
import {FileItem} from "../../../models/file-item";
import Swal from "sweetalert2";

export interface Item { name: string; url: string };

@Component({
  selector: 'app-archivos',
  templateUrl: './archivos.component.html',
  styleUrls: ['./archivos.component.scss']
})
export class ArchivosComponent implements OnInit {
  items                 : Observable<Item[]>;
  userSub            :Subscription;
  fileSub               :Subscription;
  files                    : FileItem[] = [];

  constructor(private store: Store<AppState>,   private filesService: FilesService) {

  }
  ngOnInit(): void {
    this.userSub =  this.store.select('user').pipe(
      filter( auth => auth.user !== null)
    ).subscribe(({user}) => {
      this.fileSub = this.filesService.initFilesListener(user.uid).subscribe((files) => {
      this.store.dispatch(filesActions.setFiles({files}));
     });
    });
    this.store.select('files').subscribe(({files}) => this.files = files);
  }

  eliminar(id) {
    this.filesService.eliminarArchivo(id)
      .then(() => Swal.fire('Eliminado', 'Archivo Eliminado', 'success'))
      .catch(err => Swal.fire('Error', err.message, 'error'))
  }



}

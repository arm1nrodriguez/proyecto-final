import { createReducer, on } from '@ngrx/store';
import {FileItem} from "../../../models/file-item";
import  * as action from "./archivo.actions";

export interface State {
  files: FileItem[];
}

export const initialState: State = {
  // @ts-ignore
  files: [],
}
export const _fileReducer = createReducer(initialState,
  on( action.setFiles, (state, { files }) => ({ ...state, files: [ ...files] })),
  // @ts-ignore
  on( action.unsetFiles, state => ({ ...state, files: [] })),
);

// @ts-ignore
export const fileReducer = (state, action) => _fileReducer(state, action);

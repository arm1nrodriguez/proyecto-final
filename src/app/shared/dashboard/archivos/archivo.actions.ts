import {createAction, props} from "@ngrx/store";
import {FileItem} from "../../../models/file-item";

export const setFiles     = createAction('[ARCHIVOS] SET FILES ', props<{ files: FileItem[]}>());
export const unsetFiles = createAction('[ARCHIVOS] UNSET FILES ');

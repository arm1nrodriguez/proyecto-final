import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import {InicioComponent} from "./inicio/inicio.component";
import {IngresosComponent} from "./ingresos/ingresos.component";
import {EgresosComponent} from "./egresos/egresos.component";
import {DetalleComponent} from "./detalle/detalle.component";
import {DocumentosComponent} from "./documentos/documentos.component";

const routes: Routes = [
  {
    path: '', component: DashboardComponent,
    children:  [
      { path: '', component: InicioComponent },
      { path: 'ingresos', component: IngresosComponent },
      { path: 'egresos', component: EgresosComponent },
      { path: 'detalle', component: DetalleComponent},
      { path: 'documentos', component: DocumentosComponent},
      { path: '**', redirectTo: '' }
    ]
  },


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }

import {AfterViewChecked, Component, DoCheck, OnChanges, OnDestroy, OnInit, ViewChild} from '@angular/core';
import DatalabelsPlugin from 'chartjs-plugin-datalabels';
import { ChartConfiguration, ChartData, ChartType } from 'chart.js';
import { BaseChartDirective } from 'ng2-charts';
import {Store} from "@ngrx/store";
import {AppState} from "../../../app.state";


@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.scss']
})
export class InicioComponent implements OnInit {
  @ViewChild(BaseChartDirective) chart: BaseChartDirective | undefined;

  ingresos                            = 0;
  egresos                             = 0;
  totalIngresos:  number      = 0;
  totalEgresos: number        = 0;
  saldo: number                    = 0;
  saleData                             = [];
  isData                                 = false;
  colorScheme = {
    domain: ['#d5e8f6', '#f7d4dc', '#d6f5d6',]
  };



  constructor(private store: Store<AppState>) { }

  ngOnInit(): void {
    this.store.select('ingreso').subscribe(({items}) => {
      this.montoTotalIngresos(items);
    } );
    this.store.select('egreso').subscribe(({items}) => {
      this.montoTotalEgresos(items);
    });
  }

  montoTotalIngresos(items) {
    this.ingresos         = 0;
    this.totalIngresos  = 0;
    for (const item of items) {
        this.totalIngresos += parseInt(item.monto);
        this.ingresos ++;
    }
  }

  montoTotalEgresos(items) {
    this.egresos         = 0;
    this.totalEgresos  = 0;
    this.saldo              = 0;
    for (const item of items) {
      this.totalEgresos +=  parseInt(item.monto);
      this.egresos ++;
    }
    this.saldo = this.totalIngresos - this.totalEgresos
    this.saleData = [
      { name: "Ingresos", value:  this.totalIngresos },
      { name: "Egresos", value: this.totalEgresos},
     { name: "Saldo", value:  this.saldo},
    ]

    setTimeout(()=> {
      this.isData = true;
    },3000)

  }
}

import {createAction, props} from "@ngrx/store";

export const isLoading = createAction('[UI COMPONENT] isLoading');
export const stopLoading = createAction('[UI COMPONENT] stopLoading');

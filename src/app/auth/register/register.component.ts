import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../../services/auth.service";
import Swal from "sweetalert2";
import {Router} from "@angular/router";
import {Subscription} from "rxjs";
import {Store} from "@ngrx/store";
import {AppState} from "../../app.state";
import * as ui from "../../shared/ui.actions";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit, OnDestroy {

  registerForm: FormGroup;
  uiSubscription: Subscription;
  loading = false;

  constructor(private fb: FormBuilder,
              private authService: AuthService,
              private  router: Router,
              private store: Store<AppState>) { }

  ngOnInit(): void {
    this.registerForm = this.fb.group({
      user: ['', Validators.required],
      email: ['',[Validators.required, Validators.email]],
      pw: ['', Validators.required],
    });
    this.uiSubscription = this.store.select('ui').subscribe( ui => {
      this.loading = ui.isLoading;
    })
  }

  ngOnDestroy() {

    if(this.uiSubscription){
      this.uiSubscription.unsubscribe();
    }

  }


  createUser(){
    const { user, email, pw } = this.registerForm.value;
    this.store.dispatch(ui.isLoading());
    /*
    Swal.fire({
      title: 'Creando Usuario',
      didOpen: () => {
        Swal.showLoading()
      }
    });

     */

    this.authService.createUser(user, email, pw).then( credentials => {
      console.log(credentials);
      this.router.navigate(['/']);
      this.store.dispatch(ui.stopLoading());
      //Swal.close();
    }).catch(err => {
      this.store.dispatch(ui.stopLoading());
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: err.message
      })
    });
  }

}

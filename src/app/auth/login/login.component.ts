import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../../services/auth.service";
import Swal from 'sweetalert2'
import {Router} from "@angular/router";
import {Store} from "@ngrx/store";
import {AppState} from "../../app.state";
import * as ui from "../../shared/ui.actions";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {

  loginForm: FormGroup;
  loading = false;
  uiSubscription: Subscription;

  constructor(private fb: FormBuilder,
              private authService: AuthService,
              private  router: Router,
              private store: Store<AppState>) { }

  ngOnInit(): void {
    this.loginForm = this.fb.group({
      email: ['',[Validators.required, Validators.email]],
      pw: ['', Validators.required],
    });
    this.uiSubscription = this.store.select('ui').subscribe( ui => {
      this.loading = ui.isLoading;
    })
  }

  ngOnDestroy() {

    if(this.uiSubscription){
      this.uiSubscription.unsubscribe();
    }

  }


  login() {
    const {email, pw } = this.loginForm.value;

    this.store.dispatch(ui.isLoading());

    this.authService.loginUser( email, pw).then( credentials => {
      this.store.dispatch(ui.stopLoading());
      this.router.navigate(['/dashboard']);
    }).catch(err => {
      this.store.dispatch(ui.stopLoading());
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: err.message
      })
    } );
  }

}

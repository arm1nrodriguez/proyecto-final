
import { createReducer, on } from '@ngrx/store';
import * as action from './auth.actions';
import {User} from "../models/user.model";

export interface State {
  user: User;
}


export const initialState: State = {
  // @ts-ignore
  user: null,
}



export const _userReducer = createReducer(initialState,
  on( action.setUser, (state, { user }) => ({ ...state, user: { ...user }  })),
  // @ts-ignore
  on( action.unSetUser, state => ({ ...state })),
);

// @ts-ignore
export const userReducer = (state, action) => _userReducer(state, action);

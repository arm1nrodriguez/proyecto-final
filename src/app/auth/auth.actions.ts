import {createAction, props} from "@ngrx/store";
import {User} from "../models/user.model";

export const setUser = createAction('[Auth] SET USER',
  props<{ user: User }>());

export const unSetUser = createAction('[Auth] SET USER');

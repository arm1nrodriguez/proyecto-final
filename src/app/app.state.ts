import {ActionReducerMap} from "@ngrx/store";
import * as ui from './shared/ui.reducer';
import * as auth from './auth/auth.reducer';
import * as ingreso from "./shared/dashboard/ingresos/ingreso.reducer";
import * as egreso from "./shared/dashboard/egresos/egreso.reducer";
import * as files from "./shared/dashboard/archivos/archivo.reducer";



export interface AppState{
  ui : ui.State,
  user: auth.State,
  ingreso: ingreso.State,
  egreso: egreso.State,
  files: files.State


}

export const appReducers: ActionReducerMap<AppState> = {
  ui: ui.uiReducer,
  user: auth.userReducer,
  ingreso: ingreso.ingresoReducer,
  egreso: egreso.egresoReducer,
  files: files.fileReducer,
}

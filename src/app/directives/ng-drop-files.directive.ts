import {Directive, EventEmitter, HostListener, Input, Output} from '@angular/core';
import {FileItem} from "../models/file-item";

@Directive({
  selector: '[appNgDropFiles]'
})
export class NgDropFilesDirective {

  @Input() files: FileItem[] = [];
  @Output() overMouse = new EventEmitter<boolean>();

  constructor() { }

  @HostListener('dragover', ['$event'])
  public onDragEnter(event:any){
    this.overMouse.emit(true);
    this._preventStop(event);
  }

  @HostListener('dragleave', ['$event'])
  public onDragLeave(event:any){
    this.overMouse.emit(false);
  }



  @HostListener('drop', ['$event'])
  public onDrop(event:any){
    this.overMouse.emit(false);
    const transfer = this._getTransfer(event);

    if(!transfer) {
      return;
    }

    this._getFiles(transfer.files);

    this._preventStop(event);
    this.overMouse.emit(false);



  }


  private _getTransfer(event: any){
    return event.dataTransfer ? event.dataTransfer : event.originalEvent.dataTransfer;
  }

  private _getFiles(filesList: FileList){

    for(const property in Object.getOwnPropertyNames(filesList)){
      const temporalFile =  filesList[property];

      if(this.canBeUploaded(temporalFile)){
        const newFile = new FileItem(temporalFile);
        this.files.push(newFile);
      }
    }
  }


  //Validations

  private canBeUploaded(file: File): boolean {
    return !this.isReady(file.name) && this._isImage(file.type);
  }

  private _preventStop(event: any){
    event.preventDefault();
    event.stopPropagation();
  }

  private isReady(fileName: string): boolean{
    for( const file of this.files){
      if(file.fileName === fileName){
        console.log('This File ' + fileName + 'is already uploaded!');
        return true;
      }
    }
    return false;
  }

  private _isImage (fileType: string): boolean {
    return (fileType === '' || fileType === undefined) ? false : fileType.startsWith('image');
  }
}

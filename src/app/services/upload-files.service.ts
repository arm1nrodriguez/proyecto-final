import { Injectable } from '@angular/core';
import {AngularFirestore} from "@angular/fire/compat/firestore";
import {FileItem} from "../models/file-item";

import firebase from "firebase/compat/app";
import 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
import 'firebase/database';
import {AngularFireStorage} from "@angular/fire/compat/storage";
import {AuthService} from "./auth.service";



@Injectable({
  providedIn: 'root'
})
export class UploadFilesService {

  private UPLOAD_FILES = 'img';

  constructor(private firestore: AngularFirestore, storage: AngularFireStorage, public authService: AuthService) { }

  uploadFiles(items: FileItem[]) {

    const storageRef = firebase.storage().ref();
    for(const item of items){
      item.isUpload = true;
      if(item.progress >= 100){
        continue;
      }

      const uploadTask: firebase.storage.UploadTask =
        storageRef.child(`${this.UPLOAD_FILES} / ${item.fileName}`)
          .put(item.file);
      uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
        (snapshot:any) =>
          item.progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100,
        (error: any) => console.log('Error ', error),
        () => {
          console.log('File uploaded properly');
          uploadTask.snapshot.ref.getDownloadURL()
            .then((url) => {
              item.url = url;
              item.isUpload = false;
              this.saveFile({
                name: item.fileName,
                url: item.url
              });
            });
        }
      );
    }


  }

  private saveFile(img: {name: string, url: string}){
    this.firestore.doc(`${this.authService.user.uid}/${this.UPLOAD_FILES}`)
      .collection(`items`)
      .add(img)
      .then ((ref) => {console.log('exito', ref)
      }).catch(err => console.warn(err));
  }
}

import { Injectable } from '@angular/core';
import {AngularFirestore} from "@angular/fire/compat/firestore";
import {AuthService} from "./auth.service";
import {map} from "rxjs";
import Swal from "sweetalert2";

@Injectable({
  providedIn: 'root'
})
export class FilesService {

  constructor(private firestore: AngularFirestore, public authService: AuthService) {
  }

  initFilesListener(uid: string) {
    return this.firestore.collection(`${uid}/img/items`)
      .snapshotChanges()
      .pipe(
        map(snapshot => snapshot.map(doc => ({
          uid: doc.payload.doc.id,
          ...doc.payload.doc.data() as any,
        }))));
  }

  eliminarArchivo(id: string) {
    return this.firestore.doc(`${this.authService.user.uid}/img/items/${id}`).delete();
  }
}


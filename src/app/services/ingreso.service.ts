import { Injectable } from '@angular/core';
import 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
import 'firebase/database';
import {AngularFirestore} from "@angular/fire/compat/firestore";
import {Ingresos} from "../models/ingreso.model";
import {AuthService} from "./auth.service";
import {map} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class IngresoService {

  constructor(private firestore: AngularFirestore, public authService: AuthService) { }

  crearIngreso(ingreso: Ingresos) {
    return this.firestore.doc(`${this.authService.user.uid}/ingreso`)
      .collection('items')
      .add({...ingreso});
  }

  initIngresoListener(uid: string){
    return this.firestore.collection(`${uid}/ingreso/items`)
      .snapshotChanges()
      .pipe(
        map(snapshot => snapshot.map(doc => ({
          uid: doc.payload.doc.id,
          ...doc.payload.doc.data() as any,
        }))));
  }

  eliminarIngreso(id: string) {
    return this.firestore.doc(`${this.authService.user.uid}/ingreso/items/${id}`).delete();
  }
}

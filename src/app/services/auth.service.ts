import { Injectable } from '@angular/core';
import {AngularFireAuth} from "@angular/fire/compat/auth";
import {map, Subscription} from "rxjs";
import {User} from "../models/user.model";
import {AngularFirestore} from "@angular/fire/compat/firestore";
import {Store} from "@ngrx/store";
import {AppState} from "../app.state";
import  * as action from "../auth/auth.actions";
import * as actionIngresos from "../shared/dashboard/ingresos/ingreso.actions";
import * as actionEgresos from "../shared/dashboard/egresos/egreso.actions";
import * as actionFiles from "../shared/dashboard/archivos/archivo.actions";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  userSubscription: Subscription;
  private _user: User;

  constructor(public auth: AngularFireAuth,
              public fireStore: AngularFirestore,
              public store: Store<AppState>

    ) {
  }

  get user(){
    return this._user;
  }

  initAuthListener(){
    this.auth.authState.subscribe((user => {
      if(user){
        this.userSubscription = this.fireStore.doc(`${user.uid}/user`).valueChanges()
          .subscribe((firestoreUser: any) => {
            const user = User.fromFirebase( firestoreUser );
            this._user = user;
            this.store.dispatch( action.setUser({ user }) );
          })
      }else {
        this._user = null;
        if(this.userSubscription){
          this.userSubscription.unsubscribe();
        }
          this.store.dispatch(action.unSetUser());
          this.store.dispatch(actionIngresos.unsetItems());
          this.store.dispatch(actionEgresos.unsetItems());
          this.store.dispatch(actionFiles.unsetFiles());
      }
    }))
  }

  createUser(userName: string, email: string, pw: string){
    return this.auth.createUserWithEmailAndPassword(email, pw)
      .then(( { user } ) => {
        // @ts-ignore
        const newUser = new User(user.uid, userName, user.email);

        // @ts-ignore
        return this.fireStore.doc( `${user.uid}/user`).set({...newUser});

      })
  }

  loginUser(email: string, pw: string){
    return this.auth.signInWithEmailAndPassword(email, pw);
  }

  logout() {
   return  this.auth.signOut();
  }

  isAuth(){
    return this.auth.authState.pipe(
      map((user) => user != null)
    );
  }
}

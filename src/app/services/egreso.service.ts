import { Injectable } from '@angular/core';
import {AngularFirestore} from "@angular/fire/compat/firestore";
import {AuthService} from "./auth.service";
import 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
import 'firebase/database';
import {Egresos} from "../models/egresos.model";
import {map} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class EgresoService {

  constructor(private firestore: AngularFirestore, public authService: AuthService) { }

  crearEgreso(egreso: Egresos) {
    return this.firestore.doc(`${this.authService.user.uid}/egreso`)
      .collection('items')
      .add({...egreso});
  }

  initEgresoListener(uid: string){
    return this.firestore.collection(`${uid}/egreso/items`)
      .snapshotChanges()
      .pipe(
        map(snapshot => snapshot.map(doc => ({
          uid: doc.payload.doc.id,
          ...doc.payload.doc.data() as any,
        }))));
  }

  eliminarEgreso(id: string) {
    return this.firestore.doc(`${this.authService.user.uid}/egreso/items/${id}`).delete();
  }

}

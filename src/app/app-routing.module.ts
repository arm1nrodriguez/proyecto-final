import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AuthGuard} from "./services/auth.guard";

const routes: Routes = [{ path: '',
                          loadChildren: () => import('./auth/auth.module')
                            .then(m => m.AuthModule) },
                        { path: 'dashboard',
                          canActivate:[AuthGuard],
                          loadChildren: () => import('./shared/dashboard/dashboard.module')
                            .then(m => m.DashboardModule) },


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
